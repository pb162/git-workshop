package cz.muni.fi.pb162.git;


/**
 * Program entry-point for demonstration purposes
 * @author Jakub Cechacek
 */
public class Demo {

    public static final double CZK_AMOUNT = 1000;
    public static final double FOREIGN_AMOUNT = 100;

    public static void main(String[] args) {
        Converter converter = null;

        
        if (converter != null) {
            double foreign = converter.fromCzk(CZK_AMOUNT);
            double czech = converter.toCzk(FOREIGN_AMOUNT);

            System.out.println(CZK_AMOUNT + " in czk is " + foreign + " in foreign currency");
            System.out.println(FOREIGN_AMOUNT + " in foreign currency is " + foreign + " czk");
        } else {
            System.err.println("No converter implementation found");
        }

    }


}
