package cz.muni.fi.pb162.git;

public class DollarConverter implements Converter {

    private static final double RATIO = 22;

    public double getConvertRatio() {
        return RATIO;
    }

    public double toCzk(double amount) {
        return amount * RATIO;
    }

    public double fromCzk(double amount) {
        return amount / RATIO;
    }
}
